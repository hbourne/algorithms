package cs01

import scala.annotation.tailrec

/**
 * Divide and conquer style recursive sort
 *  with logarithmic complexity
 */
object MergeSort {

  /**
   * Given two sorted sub arrays, zip them
   *   to a single sorted array
   */
  @tailrec
  private[cs01] def combine[A](as: Seq[A], bs: Seq[A], acc: Seq[A] = Nil)(implicit ord: Ordering[A]): Seq[A] = (as, bs) match {

    // a < b
    case (a :: _, b :: _) if ord.lt(a, b) =>
      combine(as.tail, bs, acc :+ a)

    // a >= b
    case (_ :: _, b :: _) =>
      combine(as, bs.tail, acc :+ b)

    // a empty
    case (Nil, _ :: _) =>
      acc ++ bs

    // b empty
    case (_, Nil) =>
      acc ++ as
  }

  def sort[A](xs: Seq[A])(implicit ord: Ordering[A]): Seq[A] = {

    // single element: identity
    if (xs.size == 1)
      xs

    // two elements: manually order them
    else if (xs.size == 2) {
      val a :: b :: _ = xs
      if (ord.lt(a, b))
        xs
      else
        Seq(b, a)
    }

    // divide / conquer / combine
    else {

      val (a, b) = xs.splitAt(xs.size / 2)
      val aSorted = sort(a)
      val bSorted = sort(b)

      val result = combine(aSorted, bSorted)
      result
    }
  }
}

