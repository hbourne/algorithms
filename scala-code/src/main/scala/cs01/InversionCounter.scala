package cs01

import scala.annotation.tailrec
import scala.io.{BufferedSource, Source}

/**
 * Count the number of element pairs where ith element is greater
 * than jth element, where i,y <= size of array
 *
 * Compared to brute force n squared time, this uses divide and
 * conquer to get logarithm complexity, by using small modification
 * of merge sort's combine op (and by leveraging sort throughout)
 *
 * Vector is used for its effectively constant time append (compared to
 *  other standard collections' logarithm time) and BigInt is used
 *  to support large number of inversions
 */
object InversionCounter {

  def countInversions[A](xs: Seq[A])(implicit ord: Ordering[A]): BigInt = countAndSort(xs)._2

  private[cs01] def countAndSort[A](xs: Seq[A])(implicit ord: Ordering[A]): (Seq[A], BigInt) = xs match {

    // Base cases : micro level neighbouring leaf pairings
    case xs if xs.size == 1 => (xs, 0)
    case Vector(x0, x1) if ord.gt(x0, x1) =>
      (Vector(x1, x0), 1)
    case _ if xs.size == 2 => (xs, 0)

    case _ =>

      println(s"Splitting array of size ${xs.size}")

      val (left, right) = xs.splitAt(xs.size / 2)

      val (sortedLeft, leftInversions) = countAndSort(left)
      val (sortedRight, rightInversions) = countAndSort(right)

      // Macro level : get info from sort routine
      val (merged, counted) = combineAndCount(sortedLeft, sortedRight)

      val result = (merged, counted + leftInversions + rightInversions)
      result
  }

  /**
   * Modification of the combine function from merge sort
   * such that it counts inversions as it goes
   *
   * Walks through a pair sorted of arrays, accumulating
   * elements by taking the lowest head of the two arrays
   *
   * Every time it draws from the second array implies an
   * inversion between that element, and every element
   * yet to be drawn from the first array
   */
  @tailrec
  private[cs01] def combineAndCount[A](as: Seq[A],
                                       bs: Seq[A],
                                       acc: Seq[A] = Nil,
                                       nInv: BigInt = BigInt(0))(implicit ord: Ordering[A]): (Seq[A], BigInt) = (as.headOption, bs.headOption) match {

    case (Some(a), Some(b)) =>
      // a < b
      if (ord.lt(a, b))
        combineAndCount(as.tail, bs, acc :+ a, nInv)

      // a >= b : the inversion case
      else
        combineAndCount(as, bs.tail, acc :+ b, nInv + as.size)

    // a empty
    case _ if as.isEmpty & bs.nonEmpty =>
      val value = acc ++ bs
      (value.toVector, nInv)

    // b empty
    case _ if bs.isEmpty =>
      val value = acc ++ as
      (value.toVector, nInv)
  }

}

