package cs01

import math.sqrt


case class Coordinate(x: Short, y: Short) {

  def euclideanDistance(that: Coordinate): Short = {

    val Coordinate(x1, y1) = that
    val (minX, maxX) = if (x1 > x) (x, x1) else (x1, x)
    val (minY, maxY) = if (y1 > y) (y, y1) else (y1, y)

    val deltaX = maxX - minX
    val deltaY = maxY - minY

    val distance = sqrt((deltaX * deltaX) + (deltaY * deltaY))

    distance.toShort
  }

  def unapply: (Short, Short) = (x, y)

}

object ClosestPair {

  // Ideally I roll my own sortBy at some point, using my merge sort
  private[cs01] def coordSort(xs: Seq[Coordinate]): Seq[Coordinate] = xs.sortBy(c => (c.x, c.y))

  private[cs01] def plusOrMinus(a:Short, b:Short, delta: Short): Boolean = {
    val min :: max :: _ = Seq(a,b).sorted
    (max - min) < delta
  }

  type Pair = ((Coordinate, Coordinate), Short)

  private [cs01] def closestSplitPair(q: Seq[Coordinate], p: Seq[Coordinate], delta: Short): Option[Pair] = {

    val xBar = q.last.x  // As sorted by (x, y), largest X of left elements is last element

    // xBar +/- delta coordinates
    val candidates: Seq[Coordinate] = for {
      list <- Seq(q, p)
      i <- Range(list.size-1, 0, -1)
      coord = list(i)
      if plusOrMinus(coord.x, xBar, delta)
    } yield coord

    // Brute force within this interval
    //   Due to definition of delta as smallest distance between points,
    //   Max elements possible here are 8
    assert(candidates.size <= 8, "Interested if this fails!")

    val allDistances: Seq[Pair] = for {
      c0 <- candidates
      c1 <- candidates if c1 != c0
    } yield ((c0, c1), c0.euclideanDistance(c1))

    allDistances.minByOption(x => x._2)  // Take pair of smallest euclidean distance
  }

  /**
   * Return the pair of coordinates with the smallest distance between them
   *
   * Recursively solves sub problems, and joins halves up by
   *  a well chosen brute force search within max 8 elements
   *  that handles case where we split across the smallest pair
   */
  def getClosestPair(xs: Seq[Coordinate]): Pair = xs match {

    // Odd array length case. for/yield using `if c1 != c0` (+ case class equality) saves us from being bitten by this
    case a :: Nil =>
      ((a, a), Short.MaxValue)

    // Base case: just a pair
    case a :: b :: Nil =>
      (a, b) -> a.euclideanDistance(b)

    case _ =>

      val sortedPoints = coordSort(xs)

      val (q, p) = sortedPoints.splitAt(sortedPoints.size / 2)

      val (qPair, dQ) = getClosestPair(q)
      val (pPair, dP) = getClosestPair(p)

      val bestCasePair @ (_, delta) = if (dQ < dP) (qPair, dQ) else (pPair, dP)

      closestSplitPair(q, p, delta)  // Worst case: a pair occurred over the split
        .getOrElse(bestCasePair)     // if not, take best of the halves
  }
}

object ClosestPairApp extends App {

  val inputPairs = Seq(

    Coordinate(1,1),
    Coordinate(1,20),
    Coordinate(1,30),

    Coordinate(3,50),
    Coordinate(4,51)

  )

  val result = ClosestPair.getClosestPair(inputPairs)
  print(result)
}