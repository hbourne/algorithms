package cs01

/**
 * Compared to "primary school" approach to multiplication
 *  of large numbers, this saves a bunch of steps by breaking
 *  each number down to their own products of smaller numbers,
 *  and using an algebra trick to skip calculating everything
 *
 * Mapping numbers to/from array of base ten digits is done with
 *  helper methods
 *
 * BigInt is used to contain the enormous values required in the
 *  Coursera assignment
 */
object Karatsuba {

  private[cs01] def leftPad(xs:List[Int], n: Int): List[Int] =
    List.fill(n - xs.size)(0) ++ xs

  // nasty
  @scala.annotation.tailrec
  private [cs01] def bigIntPow(n: BigInt, pow: Int, tot: Option[BigInt]=None): BigInt =
    if (pow < 1)
      throw new NotImplementedError("This shouldn't be required for this work; implies a bug!")
    else if (pow == 1)
      tot.getOrElse(n)
    else
      bigIntPow(n, pow-1, tot.orElse(Some(n)).map( i => i * n))

  @scala.annotation.tailrec
  private [cs01] def tobase10Array(a: BigInt, digits: List[Int]=Nil): List[Int] = {
    val r: Int = (a % 10).toInt
    if (a < 10)
      r +: digits
    else
      tobase10Array(a / 10, r +: digits)
  }

  @scala.annotation.tailrec
  private [cs01] def fromBase10Array(array: List[Int], acc: BigInt=BigInt(0)): BigInt = {
    if (array.isEmpty) {
      acc
    } else {

      val head +: tail = array
      val order = array.size - 1
      val contribution = if (order == 0) BigInt(head) else head * bigIntPow(BigInt(10), order)

      fromBase10Array(tail, contribution + acc)
    }
  }

  def multiply(x: BigInt, y: BigInt): BigInt ={
    // Base case: small numbers can be directly computed
    if(x <= 10 || y <= 10) {
      x * y

    } else {

      val xArr: List[Int] = tobase10Array(x)
      val yArr: List[Int] = tobase10Array(y)

      // Max length we're dealing with:
      val largest = Seq(xArr, yArr).map(_.size).max
      val m = math.ceil(largest.toFloat / 2).toInt
      val n = 2 * m

      // Prefix padding zeroes if needed, and split up:
      val (aArr, bArr) = leftPad(xArr, n).splitAt(m)
      val (cArr, dArr) = leftPad(yArr, n).splitAt(m)

      // Back to BigInt
      val a :: b :: c :: d :: _ = List(aArr, bArr, cArr, dArr).map(fromBase10Array(_))

      // Recurse
      val ac = multiply(a, c)
      val bd = multiply(b, d)

      val product = multiply(a+b, c+d)

      // Combine
      val gaussTrick = product - ac - bd
      val result = ac * bigIntPow(10, n) + gaussTrick * bigIntPow(10, m) + bd

      result
    }
  }
}

