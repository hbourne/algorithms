package cs01
import scala.annotation.tailrec
import scala.math.floor


/*
Todo:
  - remove 'reverse' shenanigans
  - replace multiply call with another hand rolled thing
  - replace bas10Array string method with base10 logic
 */


trait Multiplier {
  def multiply(a: Int, b: Int): Int

  private[cs01] def base10Array(a: Double): List[Int] = {
    // Better way would be a base ten calculation, revisit this later
    a.toString.toList.takeWhile(_.isDigit).map(_.toString.toInt)
  }
}

/**
 * Silly to do given we have multiply defined etc;
 * more for the sake of getting back to Scala
 */
object PrimarySchoolMultiply extends Multiplier {

  @tailrec
  private [cs01] def baseTenify(input: Seq[Int], carried: Int=0, output: Seq[Int]=Nil) : Seq[Int] =
    input match {

      case Nil if carried > 0 =>
        carried +: output

      case Nil =>
        output

      case init :+ n =>
        val sum = n + carried

        val carries = floor(sum / 10).toInt
        val remainder = sum % 10

        baseTenify(init, carries, remainder +: output)
    }

  private [cs01] def listCombine(listOne: Seq[Int], listTwo: Seq[Int]): Seq[Int] = {

      // Ensure list lengths match, as zip excludes elements
      // where one array is empty
      val paddedListTwo = if (listTwo.length > listOne.length)
        listTwo
      else {
        val zeros = Seq.fill(listOne.size - listTwo.size)(0)
        zeros ++ listTwo
      }

      val elementWiseSummed = listOne.zip(paddedListTwo).map { case (a, b) => a + b }
      elementWiseSummed
    }

  /**
   * Multiply two numbers in primary school style
   */
  override def multiply(a: Int, b: Int): Int = {

    val elementsOfA: Seq[Int] = base10Array(a).reverse  // order small -> big
    val elementsOfB: Seq[Int] = base10Array(b)

    val partResults: Seq[Seq[Int]] = elementsOfA.zipWithIndex.map { case (elementOfa, i) =>
      val product = elementOfa * b
      product +: Seq.fill(i)(0)  // append zeros
    }.reverse

    val zeroes = Seq.fill(partResults.size)(0)
    val summed: Seq[Int] = partResults.fold(zeroes)(listCombine)

    val baseTenned = baseTenify(summed)
    baseTenned.mkString("").toInt
  }
}
