package cs01

import scala.annotation.tailrec
import scala.util.Random


object PivotRules {

  type PivotRule[A] = (Int, Int, Array[A]) => Int

  def random[A]: PivotRule[A] = { (l, r, _) =>
    val n = l - r
    if (n <= 1)
      n
    else
      Random.between(1, n)
  }

  def first[A]: PivotRule[A] = (l, r, _) =>
    if (l == r)
      l           //
    else
      l + 1       //

  def last[A]: PivotRule[A] = (_, r, _) => r

//  val medianOfThree = ??? // todo requires Ordered
}


/**
 * Implementation of quick sort
 *
 * Doesn't address ties
 */
object QuickSort {

  import PivotRules._

  private[cs01]
  implicit class ArrayExtension[A](as: Array[A]) {
    def swapElements(a: Int, b: Int): Unit = {
      val valueA = as(a)
      val valueB = as(b)

      as.update(a, valueB)
      as.update(b, valueA)
    }
  }

  /**
   * Take one pass through the array, comparing each
   * element to the value of the pivot.
   *
   * Each time, if the value is lower than the pivot,
   * move where we think the pivot position is.
   *
   * Produces an array where all elements left of the
   * pivot are lower, and all elements right are greater.
   */
  @tailrec private[cs01]
  def partitionLoop[A: Ordering](as: Array[A], i: Int, j: Int, right: Int, pivotValue: A): Int =

    if (i > right) // i suggested out of bounds: right most element is new pivot value
      right
    else if (j > right) // j suggested out bounds. Accounting for the "reserved initial"
    // position, end pivot value is i -1
      i - 1
    else {
      val value = as(j)
      if (implicitly[Ordering[A]].lt(value, pivotValue)) {

        // Element > pivot: swap position with pivot
        as.swapElements(i, j)
        // Increment pivot position
        partitionLoop(as, i + 1, j + 1, right, pivotValue)
      } else
        partitionLoop(as, i, j + 1, right, pivotValue)
    }

  /**
   * Partition array about a pivot, such that all elements
   * left of it are less than the pivot element, and all
   * elements right of it are greater than the pivot element.
   *
   * Arguments [[left]], [[right]] define a restricted region
   * of the array to work in
   *
   * Pivot element is chosen randomly, but can be specified
   *
   * Returns the final position of the pivot index
   */
  private[cs01]
  def partition[A: Ordering](as: Array[A], left: Int, right: Int, forcedPivot: Option[Int] = None, pivotRule: PivotRule[A] = random[A]): (Int, Int) = {

    // Choose a pivot
    val pivotIndex: Int = forcedPivot.getOrElse(pivotRule(left, right, as))
    val pivotValue: A = as(pivotIndex)

    // Set the pivot value to the head of our region
    as.swapElements(left, pivotIndex)

    // Start the pivot off "one right" of head
    val i = left + 1
    // Compare elements from same position
    // (If first element is smaller than pivot, means we increment pivot index)
    val j = i

    val i_final = partitionLoop(as, i, j, right, pivotValue)

    // Set the pivot to new position
    as.swapElements(
      left,
      i_final // i_final needs to be able to be final element of array if required
    )

    (i, i_final - 1)
  }

  private[cs01]
  def sort[A: Ordering](as: Array[A], l: Int, r: Int, acc: Int = 0, depth: Int = 0, pivotRule: PivotRule[A] = PivotRules.random[A]): Int = {

    val n = r - l + 1 // Plus one just ensures "n=2" means 2 elements in array

    if (n < 2 || (r - l == 0))
      0

    else if (n == 2)
      implicitly[Ordering[A]].compare(as(l), as(l + 1)) match {
        case 1 =>
          as.swapElements(l, l + 1)
          1

        case _ =>
          1
      }
    else {

      val (pivotStart, pivotIndex) = partition(as, l, r, pivotRule = pivotRule)

      // Work of the partition we just did:
      val comparisonsLeft  = math.max(0, pivotStart - l - 1) // Comparisons = length - 1
      val comparisonsRight = math.max(0, r - pivotStart - 1) //   as take a single pass through

      println(s"Depth: $depth Size: $n Pivot wound up at $pivotIndex. Counted $comparisonsLeft left, $comparisonsRight right [${comparisonsLeft+comparisonsRight}]")

      val accLeft  = sort(as, l,     pivotIndex, comparisonsLeft,  depth = depth + 1, pivotRule)
      val accRight = sort(as, pivotIndex + 1, r, comparisonsRight, depth = depth + 1, pivotRule)

      acc + accLeft + accRight
    }
  }

  /**
   * Sort the array, noting how many elementwise
   * comparisons were performed.
   */
  def sortWithCounts[A: Ordering](as: Array[A], pivotRule: PivotRule[A] = random[A]): (Array[A], Int) = {
    val n = sort[A](as, 0, as.length - 1, as.length - 1, 0, pivotRule)
    (as, n)
  }

  //  /**
  //   * Sort the array.
  //   */
  //  def sort[A : Ordering](as: Array[A], pivotRule: PivotRule[A]=PivotRules.random[A]): Array[A] = {
  //    sort(as, 0, as.length-1, as.length - 1, 0 , pivotRule)
  //    as
  //  }
}

object when {

  // Seemingly these remove a little need for if else statements that return None in one branch
  val maybeOutcome: Option[String] = Option.when(1 == 1)("Who needs if else!")
  val unlessCase = Option.unless(1 == 1)("A number that is not one")
}