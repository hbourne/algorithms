package cs02

import scala.annotation.tailrec

// Singly linked node - we can traverse in one direction
case class Node1[A](data: A, protected var next: Option[Node1[A]]) {
  def mutateChild(newChild: Option[Node1[A]]): Unit = {
    this.next = newChild
  }

  def hasNext: Boolean = next.nonEmpty
  def isEmpty: Boolean = next.isEmpty
}

sealed trait Failure
case class IndexOutOfBounds(value: Int) extends Failure

sealed trait Success
case class Insertion(value: Int, isLast: Boolean) extends Success
case class InsertLast(value: Int) extends Success
case class Deletion(value: Int) extends Success

object Node1 {

  /*
  Although the functions in here superficially similar,
  I'm at peace with minor duplication, if it means not having
  crazy over-abstraction
   */

  @tailrec
  def applyForward[A](node: Node1[A], target: Int, i: Int=0): Option[Node1[A]] =
    if (i == target)
      Some(node)
    else if (node.next.isEmpty)
      None
    else
      applyForward(node.next.get, target, i + 1)

  @tailrec
  def insertForward[A](value: A, node: Node1[A], target: Int, i: Int=0): Either[IndexOutOfBounds, Insertion]  =
    if (i == target - 1) {
      val isNewLast = node.next.isEmpty
      node.mutateChild(newChild = Some(Node1(value, node.next)))
      Right(Insertion(target, isNewLast))
    }
    else if (node.next.isEmpty)
      Left(IndexOutOfBounds(i))
    else
      insertForward(value, node.next.get, target, i + 1)

  @scala.annotation.tailrec
  def deleteForward[A](node: Node1[A], target: Int, i: Int=0): Either[IndexOutOfBounds, Deletion] = {
    if (i == target - 1) {
      val grandchild = node.next.flatMap(_.next)
      node.mutateChild(newChild = grandchild)
      Right(Deletion(target))
    }
    else if (node.next.isEmpty)
      Left(IndexOutOfBounds(i))
    else
      deleteForward(node, target, i+1)
  }

  // My noble over abstraction spiel aside, what if we *did* have a generic method for walking and doing something?

  @tailrec
  def collectFirst[A, B](i: Int, node: Node1[A])(pf: PartialFunction[(Int, Node1[A]), B]): Option[B] = {
    if (pf.isDefinedAt(i, node))
      pf.lift.apply(i, node)
    else if (node.isEmpty)
      None
    else
      collectFirst(i+1, node.next.get)(pf)
  }

  // Do something at an index - or tell us it isn't there
  def indexedOp[A, B](target: Int, node: Node1[A])(pf: PartialFunction[(Int, Node1[A]), B]): Either[IndexOutOfBounds, B] =
    collectFirst(0, node)(pf).map(Right(_)).getOrElse(Left(IndexOutOfBounds(target)))

  def altApply[A](node: Node1[A], target: Int): Either[IndexOutOfBounds, A] =
   indexedOp(target, node) {
     case (i, node) if i == target => node.data
   }

  def altInsert[A](value: A, target: Int, node: Node1[A]): Either[IndexOutOfBounds, Insertion] =
    indexedOp(target, node) {
      case (i, node) if i == target - 1 =>
        val isNewLast = node.next.isEmpty
        node.mutateChild(newChild = Some(Node1(value, node.next)))
        Insertion(target, isNewLast)
    }

  def altDelete[A](node: Node1[A], target: Int, i: Int=0): Either[IndexOutOfBounds, Deletion] =
    indexedOp(target, node) {
     case (i, node) if i == target - 1 =>
       val grandchild = node.next.flatMap(_.next)
       node.mutateChild(newChild = grandchild)
       Deletion(target)
    }

}

/**
  * Data structure offering O(1)
  */
class SingleLinkedList[A]() {

  private var underlying: Option[Node1[A]] = None
  private var lastNode: Option[Node1[A]] = None    // Might be more useful for doubly linked ops
  private var currSize: Int = 0

  def isEmpty: Boolean = underlying.isEmpty
  def nonEmpty: Boolean = underlying.nonEmpty

  def apply(i: Int): Option[A] =
    if (isEmpty)
      None
    else
      Node1
        .altApply(underlying.get, i)
        .toOption

  def insert(data: A, i: Int): Either[IndexOutOfBounds, Insertion] = {

    // Use prepend as insertForward doesn't handle empty root node currently
    val outcome = if (isEmpty)
      prepend(data)
    else {
      val res = Node1.insertForward(data, underlying.get, i)
      if (res.isRight) currSize += 1
      res
    }

    // Update our last node helper
    if (outcome.exists(_.isLast))
      lastNode = Some(new Node1(data, None))

    outcome
  }

  def headOption: Option[A] = underlying.map(_.data)

  def prepend(data: A): Right[Nothing, Insertion] = {
    val startEmpty = underlying.isEmpty
    underlying = Some(Node1(data, underlying))
    lastNode = underlying
    currSize += 1
    Right(Insertion(0, startEmpty))
  }

  def append(data: A): Unit = insert(data, currSize)

  def size: Int = currSize

  def delete(i: Int): Either[IndexOutOfBounds, Deletion] =
    if (isEmpty)
      Left(IndexOutOfBounds(i))
    else
      Node1.deleteForward(underlying.get, i)

  def find(predicate: A => Boolean): Option[A] = if(underlying.isEmpty)
      None
    else
      Node1.collectFirst(0, underlying.get) {
        case (_, node) if predicate(node.data) => node.data
      }

  def exists(predicate: A => Boolean): Boolean =
    find(predicate).nonEmpty

  def contains(a: A): Boolean = exists(_ == a)

}

object SingleLinkedList {
  def empty[A] = new SingleLinkedList[A]()
}