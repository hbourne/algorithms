package cs02

/**
  * Given an unsorted array A of n integers, and a target integer t.
  * Determine whether or not there are two number x,y in A satisfying
  *   the relation x + y = t
  */
object TwoSumCalculator {

  /**
    * Solve the 2-SUM problem by looking up the difference
    * between a given element and the target
    *
    * This is a clever 1-up over brute force n square comparisons
    */
  def calculate(input: Seq[Int], t: Int): Boolean = {

    val hashTable: ChainingHashMap[Int, Int] = ChainingHashMap.empty

    // n inserts
    input.foreach { x => hashTable.insert(x, x) }

    // at most n lookups
    val maybeTarget = input.find { x =>
      val y = t - x
      hashTable.contains(y)
    }

    // yes or no answer
    maybeTarget.nonEmpty
  }
}
