package cs02.functional

case class Edge(tail: Int, head: Int) {
  def ids = tail :: head :: Nil
  def unzip = (tail, head)
}
