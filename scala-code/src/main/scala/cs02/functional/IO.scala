package cs02.functional

import scala.collection.{MapView, mutable}

object IO {

  /**
    * Read in free form graph specification of the form
    *    (1, 2),(1, 4)
    *
    * A nice excuse to try parser combinators. I strongly
    *   disagree with the terse syntax - but once you
    *   do a little they're fairly sane.
    *
    */
  def parsePairsFromString(string: String): Seq[Edge] = {

    import fastparse._
    import NoWhitespace._

    // ! captures the parsed value as return value
    def digits[_: P] = P(CharsWhileIn("0-9").!.map(_.toInt))

    // ~ means "then"
    def pair[_: P] =
      P(
        ("(" ~ digits ~ "," ~ digits ~ ")")
          .map { case (tail: Int, head: Int) => Edge(tail, head) })

    // rep means "repeating"
    def graph[_: P] = P(pair.rep(sep = ","))

    val result = parse(string, graph(_))

    if (result.isSuccess)
      result.get.value
    else {
      println(":(")
      Nil
    }
  }

  /**
    * Build up a recursive graph data structure.
    *
    *
    */
  def assembleInductive(pairs: Seq[Edge]) = {

    val idSet = pairs.flatMap(_.ids).toSet

    val tails = pairs.map(_.tail).toSet
    val heads = pairs.map(_.head).toSet

    val emptyInts: Seq[Int] = Seq.empty

    val (sources, sinks) = idSet.foldLeft((emptyInts, emptyInts)) {
      case ((sources, sinks), nodeId: Int) =>
        val isTail = tails.contains(nodeId)
        val isHead = heads.contains(nodeId)

        if (isTail & !isHead)
          (sources :+ nodeId, sinks)
        else if (isHead & !isTail)
          (sources, sinks :+ nodeId)
        else
          (sources, sinks)
    }

    val nodesByTail: MapView[Int, Seq[Int]] =
      pairs.groupBy(_.tail).view.mapValues(_.map(_.head))
    val uniqueSources: Set[Int] = sources.toSet

    // Depth vs Breadth first
    // Either way we visit all nodes
    val toVisit = new mutable.Stack[Int]()
    toVisit.addAll(uniqueSources)

    /**
      * Given a Source node.
      *
      * Eurgh assembling a graph is turning into a UCC style walk !!!
      */
    //    @tailrec
    def buildFromSource(
        tail: Int,
        heads: Seq[Int],
        //                         remainingSources: Seq[Int]=uniqueSources.toSeq,

        parentNode: Node
    ): Node = {

      // Depth bottoming out base case
      if (heads.isEmpty)
        parentNode
      else {

        val child = heads.head

        // New node
        val newNode = Node(Seq(parentNode), mutable.Seq(), tail)

        // Children of the node
        val nodeSeq =
          heads.map(child => buildFromSource(child, nodesByTail(tail), newNode))

        // Todo, the coaslesce question
        // Repeat visits to nodes will occur. How do we reduce n verions to just one?
        // This risks turning into a really nasty walk all over the graph
        // every time we visit a new node, to check if we handled it...

        // Easy case, require single outbound edges. That precludes meaingful CCs tho.

        newNode.addChildren(heads)

        ???
        //        newNode.addChildren(
        //          outboundEdges.map(_.head)
        //        )
        //
        //        buildFromSource(
        //          remainingSources.head, remainingSources.tail, parentNode
        //        )
      }
    }
  }

}
