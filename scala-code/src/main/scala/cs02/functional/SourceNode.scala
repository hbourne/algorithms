package cs02.functional

import scala.collection.mutable

class SourceNode(id: Int, outbound: mutable.Seq[Node])
    extends Node(Nil, outbound, id)
