package cs02.functional

import scala.collection.mutable

// Node in an inductive graph (that is, one built up, like List from cons :: )
case class Node(
    inbound: Seq[Node],
    var outbound: mutable.Seq[Node],
    id: Int
) {

  def addChildren(ids: Seq[Int]): Unit = {
    val nodes = ids.map { id =>
      Node(Seq(this), mutable.Seq(), id)
    }
    this.outbound  ++= nodes
  }
}
