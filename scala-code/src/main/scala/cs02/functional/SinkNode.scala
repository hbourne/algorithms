package cs02.functional

import scala.collection.mutable

class SinkNode(id: Int, inbound: Seq[Node])
    extends Node(inbound, mutable.Seq(), id)
