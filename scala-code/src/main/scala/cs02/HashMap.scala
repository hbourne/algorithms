package cs02
import scala.util.hashing.MurmurHash3

object HashFunctions {

  val scalaDefault: Any => Int = { thing: Any => thing.## }
  val murmerHash3 = ???
}


/**
  * A HashMap is a data structure that works on Key-Value
  * pairs, supporting constant time inserts and look-ups.
  *
  * Might consider extending the Scala collections API once
  * the implementations work.
  **/
trait HashMap[K, V] {

  val hashFunction: K => Int

  /**
    * Insert a value with a given key
    */
  def insert(key: K, value: V): Unit

  /**
    * Look for a value stored under a given key
    * If no value exists, return None
    */
  def lookup(key: K): Option[V]

  /**
    * Look for a value stored under a given key
    * If no value exists, return None
    */
  def apply(key: K): Option[V] = lookup(key)

  /**
    * Keep on top of load by resizing the array
    * & rehashing everything
    */
  protected def resize(): Unit

}

/**
  * HashMap implementation that resolves collisions by
  * using chaining.
  *
  * Every element of the underlying array is a 'bucket'.
  * Each insertion to a given bucket appends an element
  * to a linked list within the bucket.
  *
  * The cost of searching these linked lists can become
  * significant as the load increases
  *
  */
class ChainingHashMap[K, V](
                                    var size: Int = 100,  // todo auto sizing and start at 10 ?
                                    override val hashFunction: K => Int
                                    ) extends HashMap[K, V] {


  private var buckets: Array[SingleLinkedList[(K, V)]] = Array.fill(size)(SingleLinkedList.empty)

  override def insert(key: K, value: V): Unit = {
    val index: Int = hashFunction(key)
    val bucket = buckets(index)
    ChainingHashMap.insertIfAbsent(bucket, (key, value))
  }

  override def lookup(key: K): Option[V] = {
    val index: Int = hashFunction(key)
    val bucket = buckets(index)
    ChainingHashMap.findByTupleKey(bucket, key)
  }

  /**
    * Keep on top of load by resizing the array
    * & rehashing everything
    */
  override protected def resize(): Unit = ???

  def contains(key: K) = lookup(key).nonEmpty
}

object ChainingHashMap {

  // Todo: this is clean but inefficient. Worst case 2 scans through
  // Should probably write new method to traverse and act
  def insertIfAbsent[A](linkedList: SingleLinkedList[A], a: A): Unit =
    if (linkedList.contains(a))
      ()
    else
      linkedList.append(a)

  def findByTupleKey[K, V](linkedList: SingleLinkedList[(K, V)], k: K): Option[V] =
    linkedList.find { case element => element._1 == k }.map(_._2)

  def empty[K, V]: ChainingHashMap[K, V] = new ChainingHashMap[K, V](hashFunction = HashFunctions.scalaDefault)
}