package cs02.imperative

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

// Convention: 1 indexed arrays in this file
// Others have been 0 indexed


/**
 * Heap implementation. Exposes logarithm time insert and extractMin
 * operations.
 *
 * Classic use case being to replace a repeated min/maxby operation
 * on an evolving set of elements with use of a heap.
 *
 * A small deviation made here, in comparison to the way discussed
 * in Algorithms Illuminated, is that instead of coding it to use
 * keys for comparison, we use a context bound, requiring an ordering
 * to be provided
 */
class Heap[K : Ordering, V] {

  // Todo. I avoided using a HashMap. But this could really do with HashMap.
  //  Will probably return and update after covering the HashMap section

  // Supports both append and remove. Will keep an eye out for alternatives.
  private [imperative] val data: ArrayBuffer[(K, V)] = ArrayBuffer.empty

  def insert(k: K, v: V): Unit = {
    data.addOne((k, v))                   // Append new element to end
    Heap.bubbleUp(data, data.length - 1)  // Bubble up to correct place in tree
  }

  // We could call `as foreach insert` for O(n log n)
  def heapify(values: (K, V)*): Unit = {

    if (values.isEmpty)
      ()
    else {
      val element = values.head
      data.addOne(element)
      Heap.bubbleUp(data, data.size - 1)
      heapify(values.tail : _*)
    }
  }

  def extractMin(): Option[(K, V)] = {

    val min = data.headOption

    if (min.nonEmpty) {
      // Set the new rootnode as the last array element
      data.update(0, data.last)
      data.dropRightInPlace(1)

      // Housekeeping: make the tree regain its invariant
      //  i.e. the fact that parents are always smaller than children
      Heap.bubbleDown(data, 0)
    }

    min
  }

  /*
  This feels like it should be similar to extractMin,
    being resumed at an arbitrary point
  Vssumption: we're deleting by element value. Not by index or anything.

  Returns the old key
   */
  def delete(value: V): K = {

    // Get the index of our to be removed node in linear time
    val index = data.indexWhere(_._2 == value)
    val key = data(index)._1

    data.update(index, data.last)
    data.dropRightInPlace(1)

    // Housekeeping
    Heap.bubbleDown(data, index)
    key
  }

  def isEmpty: Boolean = data.isEmpty

  def size: Int = this.data.length

  def key(value: V): Option[K] = {
    // this is where hashing implementation is better
    data.find(_._2 == value).map(_._1)
  }
}

object Heap {

  // Assume index is zero indexed as argument

  def parent[K : Ordering, V](data: ArrayBuffer[(K, V)], index: Int): Option[(Int, K)] = {
    val oneIndexed = 1 + index
    val oneIndexedParent = math.floor(oneIndexed.toShort / 2).toInt
    val zeroIndexedParent = oneIndexedParent - 1
    Option.when(oneIndexed >= 2 && data.length >= oneIndexed)(zeroIndexedParent -> data(zeroIndexedParent)._1)
  }

  def childLeft[K : Ordering, V](data: ArrayBuffer[(K, V)], index: Int): Option[(Int, K)] = {
    val oneIndexed = 1 + index
    val oneIndexedChild = oneIndexed << 1
    val zeroIndexedChild = oneIndexedChild - 1
    Option.when(oneIndexedChild <= data.length)(zeroIndexedChild -> data(zeroIndexedChild)._1)
  }

  def childRight[K : Ordering, V](data: ArrayBuffer[(K, V)], index: Int): Option[(Int, K)] = {
    val oneIndexed = 1 + index
    val oneIndexedChild = (oneIndexed << 1) + 1
    val zeroIndexedChild = oneIndexedChild - 1
    Option.when(oneIndexedChild <= data.length)(zeroIndexedChild -> data(zeroIndexedChild)._1)
  }

  @tailrec
  def bubbleUp[K : Ordering, V](data: ArrayBuffer[(K,V)], currentIndex: Int) : Unit = {

    val maybeParent = parent(data, currentIndex)

    if (maybeParent.isEmpty)
      ()
    else {

      val (parentIndex, parentKey) = maybeParent.get
      val (currKey, _) = data(currentIndex)

      // We want the parent to be smaller. If so, we're good
      if (implicitly[Ordering[K]].lt(parentKey, currKey))
        ()

      // If not, swap the two round
      else {
        swap(data, currentIndex, parentIndex)
        bubbleUp(data, parentIndex)
      }
    }
  }

  @tailrec
  def bubbleDown[K : Ordering, V](data: ArrayBuffer[(K, V)], currentIndex: Int) : Unit = {

    // Get child indices. If no child, these may return None
    val childIndexLeft : Option[(Int, K)] = childLeft(data, currentIndex)
    val childIndexRight: Option[(Int, K)] = childRight(data, currentIndex)

    // Find the smallest child, if any
    val children: ArrayBuffer[(Int, K)] = ArrayBuffer(childIndexLeft, childIndexRight).flatten
    val smallerOption = children.minByOption(_._2) // :(

    // No children present, nothing to do.
    if (smallerOption.isEmpty)
      ()

    // Check out constraint holds
    else {

      val (childIndex, childKey): (Int, K) = smallerOption.get
      val (currKey, _): (K, V) = data(currentIndex)

      if (implicitly[Ordering[K]].lt(currKey, childKey))
        ()

      // Recurse if constraint violated
      else {
        swap(data, childIndex, currentIndex)
        bubbleDown(data, childIndex)
      }
    }
  }

  def swap[V](data: ArrayBuffer[V], i: Int, j: Int): Unit = {
    val firstElement = data(i)
    val secondElement = data(j)
    data.update(i, secondElement)
    data.update(j, firstElement)
  }

  def apply[K : Ordering, V](values: Seq[(K, V)]): Heap[K, V] = {
    val heap = new Heap[K, V]()
    heap.heapify(values :_*)
    heap
  }

  def apply[K : Ordering, V](as: (K, V)*)(implicit dummyImplicit: DummyImplicit=DummyImplicit.dummyImplicit): Heap[K, V] = apply(as)
}

