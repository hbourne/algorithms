package cs02.imperative


object Kosaraju {

  /**
    * Explore all nodes connected to [[node]],
    *   returning node ids in ascending order of distance
    */
  def dfs(node: Node, graph: Graph, reverse: Boolean=false, explored: Array[Boolean], ordering: Array[Int]): Array[Int] = {

    explored.update(node.id, true)

    // Support reverse traversal
    val children = if (reverse) node.inboundEdges else node.outboundEdges

    val orderingAfterChildren = children.foldLeft(ordering){ case (ordering, childId) =>

      val child = graph.nodes(childId)

      // Don't explore nodes we've visited
      if (explored(child.id))
        dfs(child, graph, reverse, explored, ordering)
      else
        ordering
    }

    // Return deeper nodes after shallower
    node.id +: orderingAfterChildren
  }

  /**
    * Work out a topological sort on a DAG,
    *   returning node ids in ascending f order
    */
  def topoSort(graph: Graph, reverse: Boolean): Array[Int] = {
    val explored: Array[Boolean] = Array.fill(graph.nodes.length)(false)
    val ordering = Array.empty[Int]

    graph.nodes.foldLeft(ordering){ case (componentOrdering, node) =>
      dfs(node, graph, reverse, explored, componentOrdering)
    }
  }

  /**
    * Explore all nodes connected to [[node]],
    *   marking their SCC as the current SCC.
    *
    * A variation on [[dfc]]; minor duplication
    * beats over abstraction
    */
  def dfsScc(node: Node, graph: Graph, explored: Array[Boolean], currentScc: Int, scc: Array[Int]): Unit = {

    explored.update(node.id, true)
    scc.update(node.id, currentScc)

    node.outboundEdges.collect { case node if ! explored(node) =>
      val child = graph.nodes(node)
      dfsScc(child, graph,  explored, currentScc, scc)
    }
  }

  def confirmDag(graph: Graph) = () // This would be nice to have

  /*
   * Compute the strongly connected components of a DAG
   *  represented with adjacency lists in linear time.
   *
   * Starts off by working out a topological ordering
   *  of the reverse of the graph, which yields us
   *  the sink SCC to start an exploration from.
   *
   * The exploration consists in walking up that
   *  reverse topological ordering, invoking BFS;
   *  each BFS call marks up a strongly connected
   *  component.
   *
   * Returns the SCC label of each node, represented
   *  as an Array with the same order as the nodes
   *  provided as input.
   */
  def computeComponents(graph: Graph): Array[Int] = {

    confirmDag(graph)

    val fValuesReversed = topoSort(graph, reverse=true)  // This lets us explore from the sink SCC up

    val explored = Array.fill(graph.nodes.length)(false)
    val sccs = Array.fill(graph.nodes.length)(-1)        // SCCs as mutable array reads cleaner than folding it through

    val initialScc = 0
    fValuesReversed.foldLeft(initialScc) {
      case (sccNumber, nodeId) =>

        val node = graph.nodes(nodeId)

        // Visit all nodes on a new SCC
        if (! explored(nodeId)) {
          val newScc = sccNumber + 1
          dfsScc(node, graph, explored, newScc, sccs)
          newScc
        }
        // Skip an explored SCC
        else
          sccNumber
    }

    sccs
  }
}


