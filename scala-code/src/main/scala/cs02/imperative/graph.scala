package cs02.imperative

/*
 * Nodes have an id, and ids of their Edges
 */
case class Node(id: Int, outboundEdges: Array[Int] = Array(), inboundEdges: Array[Int] = Array())

/*
 * Edges have an id, and ids of each end
 */
case class Edge(id: Int, tail: Int, head: Int, distance: Float=1f) {
  def reverse: Edge = copy(id, head, tail, distance)
}

case class Graph(nodes: Array[Node], edges: Array[Edge])


/*

/**
  * Utils for instantiating a graph from adjacency file
  */
object GraphBuilder {

  // TODO finish the rewrite of these


  def

  def fromAdjacencyLines(lines: Seq[(Int, Seq[Int])]) = {


    val nodesWith



  }




  def fromNTuples(tuples: Seq[(Int, Seq[Int])]): Graph = {

    val uniqueOutbound = tuples.flatMap(_._2)

    val (nodes, edges, _) = tuples.foldLeft(Array[Node](), Array[Edge](), 0: Int) {
      case ((nodeAcc, edgeAcc, edgeId), (nodeId, outboundNodes)) =>

        val inboundNodes = ??? // Tricky...

        val node = Node(nodeId, outboundNodes.toArray)

        // outbound edges
        val edges = outboundNodes.zipWithIndex.map {
          case (head, x) => Edge(edgeId + x, nodeId, head)
        }

        (nodeAcc :+ node, edgeAcc ++ edges, edgeId + edges.size)
    }

    // Making this parsing bit efficient can come later
    val sources = nodes.view.map(_.id)

    val sinks = uniqueOutbound.collect { case nodeId if !sources.contains(nodeId) => Node(nodeId, Array()) }

    Graph(nodes ++ sinks, edges)
  }

  def from2Tuples(tuples: Array[(Int, Int)]): Graph = {

    val asEdges = tuples.zipWithIndex.map { case ((t, h), i) => Edge(i - 1, h, t) }

    val groupedByHead: Map[Int, Array[Edge]] = asEdges.groupBy(_.head)
    val groupedByTail: Map[Int, Array[Edge]] = asEdges.groupBy(_.tail)

    val size = 11 /// gona remove this whole method soon anyway
    val nodes = (0 to size).map { idx =>

      val outbound = groupedByHead.getOrElse(idx, Array.empty).map(_.tail)
      val inbound = groupedByHead.getOrElse(idx, Array.empty).map(_.head)

      Node(idx, outbound, inbound)
    }.toArray

    Graph(nodes, asEdges)


  }#

  */
