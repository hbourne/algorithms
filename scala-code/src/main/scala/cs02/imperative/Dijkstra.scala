package cs02.imperative

import scala.annotation.tailrec

/**
  * Graph algorithm that, given a directed graph with edges of arbitrary
  * positive length, and a starting node, works out the shortest path
  * from the starting node to every other node in the graph.
  *
  * If a node is not reachable from the starting node (for instance, if
  * the starting node is in a strongly connected component that is
  * downstream of the node), then the distance to that node will be infinity.
  *
  * At a high level, the algorithm walks a path outward from the starting node.
  * It considers edge length of all outbound nodes, and takes the shortest.
  * These shortest paths are returned as a list of (distance, node).
  *
  * This implementation uses a Heap, aiming for O ( (m+n) log (n) ) runtime,
  * whereas a naive implementation (lots of distance.min) would achieve O (n squared).
  * Reality - probably bit slower than the aim, a few more improvements needed
  * on the heap implementation still...
  */
object Dijkstra {

  @tailrec
  def scc(graph: Graph,
          heap: Heap[Float, Node],
          distances: Seq[(Float, Node)]): Seq[(Float, Node)] = {

    if (heap.isEmpty)
      distances
    else {

      // Extract the nearest current node off the heap.
      // Because we recursively walk the shortest step possible, this will always be
      // the shortest path to an element. This value is appended to list of return values
      val tuple @ (lengthToW, w) = heap.extractMin().get

      // Housekeeping: some nodes may now have a shorter path found
      w.outboundEdges.foreach { edgeId =>
        val edge = graph.edges(edgeId)
        val visitedNode = graph.nodes(edge.head)

        // Remove prior distance. (Infinity, or something concrete)
        val priorKey = heap.delete(visitedNode)

        // Set it as previous, or as new path - whichever is smaller
        val newKey = Seq(priorKey, lengthToW + edge.distance).min

        heap.insert(newKey, visitedNode) // Set the new value
      }

      scc(graph, heap, distances :+ tuple)
    }
  }

  def calculateShortestPaths(graph: Graph,
                             startingNode: Node): Seq[(Float, Node)] = {

    // Mark all other nodes as infinity
    val nonStartingNodes = graph.nodes
      .collect {
        case node if node != startingNode => Float.PositiveInfinity -> node
      }

    val initialisedNodes
      : Seq[(Float, Node)] = (0f, startingNode) +: nonStartingNodes.toSeq

    val heap = Heap(initialisedNodes)
    scc(graph, heap, Nil)
  }

}
