package cs03.greedy.mst

case class Vertex(
                   id: Int,
                   edges: Array[Int]
                 )

case class Edge(
                 id: Int,
                 weight: Int,
                 vertexIds: Seq[Int] = Nil,
               )

/**
  * Adjacency list representation of a graph
  */
case class AdjacencyList(
                          vertices: Seq[Vertex],
                          edges: Seq[Edge]
                        )
