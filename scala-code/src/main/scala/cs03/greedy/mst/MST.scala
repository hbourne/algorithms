package cs03.greedy.mst

/**
  * A (finished) minimum spanning tree is a sequence of edges,
  * that covers every node in a connected graph, spanning
  * the smallest possible distance
  */
case class MST(
                path: Vector[Edge] = Vector.empty  // Vector for fast appends
              ) {

  def size: Int = path.size

  // Immutably growth
  def appendToPath(edge: Edge): MST = copy(path :+ edge)
}
