package cs03.greedy.mst

import cs02.imperative.Heap

import scala.annotation.tailrec


// O(mlogn) MST using Heaps
object HeapPrim {

  def calculateMST(graph: AdjacencyList): MST = {

    // Seed the exploration with a random vertex
    val startVertex = graph.vertices.head

    val heap = new Heap[Int, Vertex]()

    // Setup the Heap with the remaining vertices
    graph.vertices.tail.foreach { vertex =>

      val frontierEdge = graph.edges.find(_.vertexIds.sameElements(Seq(startVertex.id, vertex)))

      if (frontierEdge.nonEmpty)
        heap.insert(frontierEdge.get.weight, vertex)

      else
        heap.insert(Int.MaxValue, vertex)  // Most vertices start at +infinity
    }

    prim(graph, heap, MST())
  }

  @tailrec
  private[cs03] def prim(graph: AdjacencyList, heap: Heap[Int, Vertex], mst: MST): MST =

    // Halt once all vertices are accounted for.
    if (heap.isEmpty)
      mst

    else {

      // Find the nearest Vertex via the heap
      val (weight, vertex) = heap.extractMin().get

      // Assume only 1 edge with given length. (We've not handled ties here)
      // Add this edge to our MST
      val (Array(shortestEdge), adjacent) = vertex.edges
        .map(graph.edges)          // lookup
        .span(_.weight == weight)  // span

      mst.appendToPath(shortestEdge)

      // Pay the piper: our frontier just extended, and a bunch of nodes previously
      // at distance Int.MAXVALUE now need to update to the value of the new edge
      adjacent.foreach { edge =>

        // Take the vertex (that isn't what we just took from the Heap)
        val adjacentVertex = graph.vertices(edge.vertexIds.find(_ != vertex.id).get)

        // Update the value in the Heap
        if (edge.weight < heap.key(adjacentVertex).get) {
          heap.delete(adjacentVertex)
          heap.insert(edge.weight, adjacentVertex)
        }
      }

      prim(graph, heap, mst)
    }
}