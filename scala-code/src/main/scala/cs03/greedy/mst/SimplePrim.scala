package cs03.greedy.mst

import scala.annotation.tailrec


/**
  * ~O(m*m) implementation of Prim's algorithm for Minimum Spanning Tree (MST).
  *
  * Very close to Dijkstra's single source shortest path algorithm.
  * Snake a path forward through the graph, each step greedily taking
  * the edge with shortest length, from ANY node. (This is a different
  * choice to Dijkstra, which instead takes the edge that minimises length
  * with respect to source node).
  */
object SimplePrim {

  def calculateMST(graph: AdjacencyList): MST = {

    // Seed the exploration with a random vertex
    val startVertex = graph.vertices.head.id

    prim(graph, MST(), Set(startVertex))
  }

  @tailrec
  private[cs03] def prim(graph: AdjacencyList, mst: MST, visitedVertices: Set[Int]): MST =

    // Add an edge m times, i.e. we halt at point of fully connectedness.
    if (mst.size == (graph.vertices.length + 1))
      mst

    else {

      // Consider edges spanning the visited/unvisited frontier, O(m)
      val frontier: Seq[Edge] = graph.edges.filter(edge =>
        edge.vertexIds.count(visitedVertices.contains) == 1
      )

      // Greedily take the shortest, O(<m)
      val shortestEdge: Edge = frontier.minBy(_.weight)

      // Mark as visited, O(c)
      val visitedNew = visitedVertices + shortestEdge.id
      // Could also remove it from the AdjacencyGraph list. Unimplemented for now.

      // Update the MST, O(c)
      mst.appendToPath(shortestEdge)

      // Overall ~O(m) work per step
      prim(graph, mst, visitedNew)
    }
}
