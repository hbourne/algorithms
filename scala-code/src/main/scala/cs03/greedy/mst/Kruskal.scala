package cs03.greedy.mst

import scala.annotation.tailrec
import scala.reflect.ClassTag

sealed trait Outcome

class Failed extends Outcome

class NoSuchObject[A](a: A) extends Failed

/**
  * DataStructure that, for a static (unchanging) dataset,
  * provides 3 operations:
  *   1. Initialise - setup internal book keeping
  *   2. Find       - return the Set ID of the requested object
  *   3. Union      - merge the sets of two objects
  *
  * Used here for Kruskal's algorithm, which greedily builds
  * an MST by exploring shortest graph edges globally, and
  * merging subgraphs as edges are explored
  */
class UnionFind[A: ClassTag](data: Array[A]) {

  // Initialise. All operations are lookups and updates: Array is ideal
  private val underlying: Array[A] = data // Do we need a .clone? Not for my expected usage for now...
  private val parentGraph: Array[Int] = data.indices.toArray
  private val subTreeSize: Array[Int] = Array.fill(data.length)(1)

  def find(a: A): Either[NoSuchObject[A], Int] = {
    val i = underlying.indexOf(a)

    if (i == -1)
      Left(new NoSuchObject(a))
    else
      Right(findByIndex(i))
  }

  @tailrec
  private[cs03] final def findByIndex(i: Int): Int = {
    val parent = parentGraph(i)

    if (parent == i)
      i
    else
      findByIndex(parent)
  }

  /**
    * Merge the sets containing the given elements.
    *
    * One set becomes a child in the parent tree of the
    * other set. As doing this increases the distance
    * traversed by [[find]] by 1, we choose the smaller
    * set to become the child when we do this.
    *
    * If an element is called that does not exist, the
    * NoSuchObject from [[find]] is returned
    */
  def union(x: A, y: A): Either[NoSuchObject[A], Unit] =
    for {
      i <- find(x)
      j <- find(y)
    } yield Right {
      if (i != j) {

        val (indexBig, indexSmall) =
          if (subTreeSize(i) >= subTreeSize(j))
            (i, j)
          else
            (j, i)

        parentGraph(indexSmall) = indexBig
        subTreeSize(indexBig) = subTreeSize(i) + subTreeSize(j)
      }
    }
}

/**
  * Calculate the Minimum Spanning Tree of a fully
  * connected graph, by greedily exploring shortest
  * length Edges first, and progressively fusing
  * subtrees
  */
object Kruskal {

  def calculateMST(graph: AdjacencyList): MST = {
    val mst = MST()
    val unionFind = new UnionFind(graph.vertices.toArray)
    val sortedEdges = graph.edges.sortBy(_.weight)
    kruskal(unionFind, sortedEdges, graph, mst)
  }

  @tailrec
  private def kruskal(unionFind: UnionFind[Vertex], sortedEdges: Seq[Edge], graph: AdjacencyList, mst: MST): MST =
    if (sortedEdges.isEmpty)
      mst
    else {

      val edge@Edge(_, _, id1 :: id2 :: Nil) = sortedEdges.head
      val vertex1 = graph.vertices(id1)
      val vertex2 = graph.vertices(id2)

      // Only proceed with fusion if nodes are not already connected
      val newMst = if (unionFind.find(vertex1) != unionFind.find(vertex2)) {
        unionFind.union(vertex1, vertex2)
        mst.appendToPath(edge)
      } else
        mst

      kruskal(
        unionFind,
        sortedEdges.tail,
        graph,
        newMst
      )
    }
}
