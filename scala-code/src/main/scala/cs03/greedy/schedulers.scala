package cs03.greedy

import scala.annotation.tailrec


case class Job(
              length: Int,
              weight: Int,
              )


case class Schedule(
                   jobs: Seq[Job]
                   )

object ScheduleObjective {

  def minWeightedCompletionTimes(
                                  schedule: Schedule
                                ): Int = summation(schedule.jobs)

  def minWeightedCompletionTimes(jobs: Seq[Job]): Int = summation(jobs)

  @tailrec
  private[cs03] def summation(jobs: Seq[Job], accTime: Int=0, accSummation: Int=0): Int = jobs match {
    case Nil =>
      accSummation
    case job :: remainder =>
      val newAccTime = job.length + accTime
      val contribution = job.weight * newAccTime
      summation(
        remainder,
        newAccTime,
        accSummation + contribution
      )
  }
}

sealed trait Scheduler {
  def plan(jobs: Seq[Job]): Option[Schedule]
}

// None of these explicitly handle ties

object ExhaustiveScheduler extends Scheduler {

  override def plan(jobs: Seq[Job]): Option[Schedule] = {
    // Scala standard lib makes this too easy!
    val optimal = jobs.permutations.minByOption(ScheduleObjective.minWeightedCompletionTimes)

    optimal map Schedule
  }

}

object GreedyDiff extends Scheduler {

  private def scoreJob(job: Job) = job.weight - job.length

  override def plan(jobs: Seq[Job]): Option[Schedule] = {
    val sortDesc = jobs.sortBy(-scoreJob(_))
    Some(
      Schedule(sortDesc)
    )
  }

}

object GreedyRatio extends Scheduler {

  private def scoreJob(job: Job) = job.weight - job.length

  override def plan(jobs: Seq[Job]): Option[Schedule] = {
    val sortDesc = jobs.sortBy(-scoreJob(_))
    Some(
      Schedule(sortDesc)
    )
  }

}
