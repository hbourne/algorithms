package cs03.greedy.huffman

import scala.annotation.tailrec


/**
  * Barebones, unoptimised implementation of Huffman Encoding
  *
  * This algorithm looks for the optimal variable length, prefix-free
  *  encoding of a dataset. (i.e. an encoding that can be unambiguously
  *  decoded, and which looks to represent more frequently occurring
  *  symbols with fewer bits, for total smaller compressed size)
  **/
object QuadraticHuffman {

  def buildSigmaTree[A](symbolFrequencies: Map[A, Float]): BinaryNode[A] = {

    // Create a 'forest' of single node trees
    val initialForest: Map[BinaryNode[A], Float] = symbolFrequencies.map {
      case (k: A, v) => (BinaryNode(Some(k)), v)
    }

    // Merge them from the bottom up
    successiveTreeMerge(initialForest)
  }

  // Merge two subtrees by creating an unlabelled node, with trees as children
  private [cs03] def merge[A](node1: BinaryNode[A], node2: BinaryNode[A]): BinaryNode[A] =
    BinaryNode(None, left=Some(node1), right=Some(node2))

  /**
    * Perform ~O(n) work, n-1 times
    *
    * Looks for tree merges that contribute the smallest
    *  increase in symbol frequency probability
    *
    * Intuition: this prioritises placing heavily used symbols
    * at a shallow position in the tree, letting us achieve
    * fewer bits per symbol for compressing the dataset
    */
  @tailrec
  private[cs03] def successiveTreeMerge[A](forest: Map[BinaryNode[A], Float]): BinaryNode[A] =
    if (forest.size == 1)
      forest.head._1
    else {
      val (t1, p1) = forest.minBy(_._2)
      val (t2, p2) = forest.view.filterNot(_._1 == t1).minBy(_._2)

      val shrunkenForest = forest.removedAll(Seq(t1, t2))
      val t3 = merge(t1, t2)
      val grownForest = shrunkenForest + ((t3, p1 + p2))
      successiveTreeMerge(grownForest)
    }
}

