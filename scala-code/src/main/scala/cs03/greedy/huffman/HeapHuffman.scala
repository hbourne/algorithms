package cs03.greedy.huffman

import cs02.imperative.Heap

import scala.annotation.tailrec


/**
  * Huffman encoding in O(nlogn) time via a Heap
  **/
object HeapHuffman {

  def buildSigmaTree[A](symbolFrequencies: Map[A, Float]): BinaryNode[A] = {

    // Create a 'forest' of single node trees
    val heapForest = new Heap[Float, BinaryNode[A]]()
    symbolFrequencies.foreach {
      case (k: A, v) =>
        heapForest.insert(v, BinaryNode(Some(k)))
    }

    // Merge them from the bottom up
    successiveTreeMerge(heapForest)
  }

  // Merge two subtrees by creating an unlabelled node, with trees as children
  private [cs03] def merge[A](node1: BinaryNode[A], node2: BinaryNode[A]): BinaryNode[A] =
    BinaryNode(None, left=Some(node1), right=Some(node2))

  /**
    * Perform ~O(log n) work, n-1 times
    *
    * Looks for tree merges that contribute the smallest
    *  increase in symbol frequency probability
    *
    * Intuition: this prioritises placing heavily used symbols
    * at a shallow position in the tree, letting us achieve
    * fewer bits per symbol for compressing the dataset
    */
  @tailrec
  private[cs03] def successiveTreeMerge[A](forest: Heap[Float, BinaryNode[A]]): BinaryNode[A] =
    if (forest.size == 1)
      forest.extractMin().get._2
    else {

      val (p1,t1) = forest.extractMin().get
      val (p2,t2) = forest.extractMin().get

      val t3 = merge(t1, t2)
      forest.insert(p1 + p2, t3)
      successiveTreeMerge(forest)
    }
}

