package cs03.greedy.huffman


/**
  * O(nlogn) Huffman Encoding
  *
  * This algorithm looks for the optimal variable length, prefix-free
  *  encoding of a dataset. (i.e. an encoding that can be unambiguously
  *  decoded, and which looks to represent more frequently occurring
  *  symbols with fewer bits, for total smaller compressed size)
  **/
object SortingHuffman {

  def defaultSort[A](a: Seq[(A, Float)]): Seq[(A, Float)] = a.sortBy(_._2)

  // Parameter 'sorting' allows domain specific linear sorting e.g. radixsort instead of
  // default general purpose sort
  def buildSigmaTree[A](symbolFrequencies: Map[A, Float],
                        sorting: Seq[(A, Float)] => Seq[(A, Float)]): BinaryNode[A] = {

    // Create a 'forest' of single node trees
    // Preprocess them with n log n time (sorting)
    val initialForest: Seq[BinaryNode[A]] = sorting(symbolFrequencies.toSeq)
      .map {
        case (k: A, v) => BinaryNode(Some(k))
    }

    // n-1 merge operations, applied to the ordered nodes
    initialForest.reduce[BinaryNode[A]](merge)
  }

  // Merge two subtrees by creating an unlabelled node, with trees as children
  private [cs03] def merge[A](node1: BinaryNode[A], node2: BinaryNode[A]): BinaryNode[A] =
    BinaryNode(None, left=Some(node1), right=Some(node2))

}

