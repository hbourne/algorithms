package cs03.greedy.huffman

/**
  * A binary tree for huffman encoding
  *
  * @param symbol - can be None in case of internal, non leaf nodes
  * @param left   - left child
  * @param right  - right child
  * @tparam A     - kind of thing the tree plays with
  */
case class BinaryNode[A](
                          symbol: Option[A],
                          left: Option[BinaryNode[A]]=None,
                          right: Option[BinaryNode[A]]=None
                        )
