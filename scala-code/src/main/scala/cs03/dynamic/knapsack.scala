package cs03.dynamic

case class Item(
               size: Int,
               value: Int
               )

/**
  * Given items with size and value, and a capacity, find
  * the combination of items that maximises overall value.
  *
  * This is a dynamic programming solution
  *
  * The recursion sub problem is that, given an arbitrary order
  * on our items, and considering increasingly long prefixes of
  * the set, each time we add an item, it either appends to
  * the solution for i-1 size, or it doesn't. We explore forward
  * through this recursion, each time getting fast results from
  * an array lookup in a cache. This exploration takes O(nC) time,
  * and is the dominator of this algorithm.
  *
  * Only once the end of the exploration happens, can we "collapse
  * the wave function" and say that we know whether the final item
  * contributes to an optimal combination. A post processing step
  * walks backward through the cache array, each time accepting
  * or rejecting items based on capacity left, and whether skipping
  * an item is worth it (maybe the adjacent item has better value/size
  * ratio)
  */
class knapsack {

  def calculate(
               items: Seq[Item],
               capacity: Int
               ): Set[Item] = {

    val subSolutions = subProblems(items, capacity)
    reconstruct(items, subSolutions)
  }

  /**
    * Work out the max value for each size of sub problem.
    * Each n+1 solution is then either the solution of the
    * previous step, or includes contribution of the new item;
    * we pick the case that maximises the overall Value.
    *
    * Explore this, for each prefix set, and for all possible
    * capacities.
    *
    * Avoid lots of pointless calculation by looking up prefix
    * solutions in a cache.
    */
  def subProblems(items: Seq[Item], C: Int): Array[Array[Int]] = {

    val n = items.size

    // 2d cache.
    // Tracks the maximum sack Value at a given number of items
    val a: Array[Array[Int]] = Array.fill(n+1, C + 1)(-1)

    // Base case
    // We could just init everything to 0 instead
    0 to C foreach { c =>
      a(0)(c) = 0
    }

    1 to n foreach { i =>
      0 to C foreach { c =>

        val item = items(i)

        // Item too big: reuse preceding solution
        if (item.size > c)
          a(i)(c) = a(i-1)(c)

        // Otherwise - take best by overall value
        else
          a(i)(c) = math.max(
            a(i-1)(c),
            a(i-1)(c-item.size) + item.value
          )
      }
    }

    a
  }

  // Postprocess the explored max item Values into a solution
  private[cs03] def reconstruct(items: Seq[Item], a: Array[Array[Int]]): Set[Item] = {
    val n = items.size

    var S = Set.empty[Item]
    var C = 0

    // Append to result set only if an item fits in remaining capacity,
    // and performs equal to or better than the next item's contribution
    val exploreCandidate = (i: Int) => {
      val item = items(i)
      val enoughRoom = item.size < C

      // Peak at the next optimal value - maybe we should actually skip this guy
      val betterThanSkipping = a(i - 1)(C - item.size) + item.value >= a(i - 1)(C)

      if (enoughRoom && betterThanSkipping) {
        S = S + item
        C = C - item.size
      }
    }

    n to 1 foreach exploreCandidate

    S
  }
}
