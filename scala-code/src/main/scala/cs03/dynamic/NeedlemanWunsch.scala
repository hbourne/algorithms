package cs03.dynamic

// Todo, this guy needs tests, I don't trust it yet

/**
  * Use dynamic programming to calculate a
  * classic sequence alignment score.
  *
  * An optimal solution views the final column
  * in both solutions as one of:
  *
  * i)           Exact mapping or equivalence
  * X' :: x_m
  * Y' :: y_n
  * ii)          Insertion in X
  * X' :: [_]
  * Y' :: y_n
  * iii)         Insertion in Y
  * X' :: x_m
  * Y' :: [_]
  *
  * where X', Y' are optimal solutions to sub
  *  problems. The question of 'which one of the
  * cases is correct' is answered as 'that which
  * minimises the total penalty'.
  *
  * A forward pass through the two arrays builds
  * the optimal, minimally penalised alignment at
  * each prefix size.
  *
  * A backward pass takes the optimal full size
  * alignment, and follows the responsible branch
  * backward, selecting elements responsible.
  *
  */
object NeedlemanWunsch {

  def score(x: Array[Char],
            y: Array[Char],
            penalties: Map[Char, Map[Char, Float]],
            gapPenalty: Float
           ): Float = {
    val subSolutions = subProblems(x, y, penalties, gapPenalty)
    reconstruct(x, y, penalties, gapPenalty, subSolutions)
  }

  /**
    * Considering progressively longer array subsets each
    * time, record the overall penalty of the sequence that
    * achieved that length in the least penalised way
    *
    * Each of these builds on the result of the i-t 'th iteration
    * which is maintained in a 2d array
    */
  private[cs03] def subProblems(x: Array[Char],
                                y: Array[Char],
                                penalties: Map[Char, Map[Char, Float]],
                                gapPenalty: Float
                               ): Array[Array[Float]] = {

    val m = x.length
    val n = y.length

    // Cache, containing optimal alignment penalties
    val a = Array.fill(m, n)(-1f)
    0 to m foreach (i => a(i)(0) = i * gapPenalty)
    0 to n foreach (j => a(0)(j) = j * gapPenalty)

    1 to m foreach (i =>
      1 to n foreach { j =>

        val case1 = a(j - 1)(j - 1) + penalties(x(i))(y(j))
        val case2 = a(i - 1)(j) + gapPenalty
        val case3 = a(i)(j - 1) + gapPenalty

        a(i)(j) = Seq(case1, case2, case3).min
      }
    )

    a
  }


  private def reconstruct(x: Array[Char],
                          y: Array[Char],
                          penalties: Map[Char, Map[Char, Float]],
                          gapPenalty: Float,
                          a: Array[Array[Float]]
                         ): Float = {

    val m = x.length
    val n = y.length

    var P = 0f // penalty

    var i = m
    var j = n

    while (i > 1 && j > 1) {

      val prefixSeqPenalty = a(i)(j) // The cost of the optimal solution found (over full seq)
      val mappingPenalty = penalties(x(i))(y(j)) // The cost of mapping two current characters

      // If the sub problem score is the last score, plus a mapping penalty,
      // this is our 'case 1', direct mapping
      if (prefixSeqPenalty == a(j - 1)(j - 1) + mappingPenalty) {

        P += mappingPenalty
        i = i - 1
        j = j - 1

        // Similarly, case 2
      } else if (prefixSeqPenalty == a(i - 1)(j) + gapPenalty) {

        P += gapPenalty
        i -= 1

        // Similarly case 3
      } else if (prefixSeqPenalty == a(i)(j - 1) + gapPenalty) {

        P += gapPenalty
        j -= 1

      } else {
        println("Oh dear")
      }
    }

    P
  }
}
