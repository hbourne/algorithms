package cs03.dynamic

import java.util.UUID

import scala.annotation.tailrec


case class Vertex(
                   weight: Int,
                   id: UUID = UUID.randomUUID()
                 )

/**
  * Given a path graph - a series of nodes, linked in serial,
  * with no cycles and basically in a straight line - calculate
  * the combination of non adjacent nodes that provides the
  * maximal total weight overall.
  *
  * Do this by using dynamic programming:
  *  1. Work out the size of the optimal WIS solution, by
  *     splitting the problem into sub problems, each of which
  *     contributes the answer of its super problem
  *  2. Re use the sub problem answers to reconstruct
  *     the optimal answer
  *
  *  Caching of repeatedly tried results makes for nice ~O(n) runtime,
  */
object WIS {

  def calculate(pathGraph: Array[Vertex]): Set[Vertex] = {
    val subgraphSolutions = maxSubgraphWeights(pathGraph)
    reconstruct(pathGraph, subgraphSolutions)
  }

  /**
    * Starting with the smallest path (size 2), and up to the largest path,
    * work out the greatest possible sum of weights at that length.
    *
    * This always involves a choice:
    *  - it doesn't include the current vertex
    *  - does include the current vertex
    *
    * Each answer to this question is cached, and lookup in answering
    *  the i+1'th solution.
    *
    * Returns the array of 'max sum weight at current length' values.
    */
  private def maxSubgraphWeights(pathGraph: Array[Vertex]): Array[Int] = {

    val n = pathGraph.length
    val a = Array.fill(n + 1)(-1)
    a(0) = 0
    a(1) = pathGraph(1).weight

    2 to n foreach { i =>

      // Each time, the optimal solution is either the same as the
      // previous solution, or it includes the new node contribution

      a(i) = math.max(
        a(i-1),                       // prev solution
        a(i-2) + pathGraph(i).weight  // new node included
      )
    }

    a  // a(n) is the total weight of max solution
  }

  /**
    * Given the array containing the maximal value of the WIS at
    *  every sub problem length, walk backwards through each of
    *  these values, comparing, and deciding at each step whether
    *  to take the current vertex and skip to the next non adjacent
    *  vertex, or to recurse to the next adjacent one
    */
  @tailrec
  private def reconstruct(vertices: Array[Vertex], a: Array[Int], i: Int=2, result: Set[Vertex]=Set.empty): Set[Vertex] = {

    val vertex = vertices(i)
    if (i == 1)
      result + vertex

    // Follow the optimal solutions

    else if (a(i-1) >= a(i-2) + vertex.weight)   // Case without the current vertex weighed more
      reconstruct(vertices, a, i - 1, result)
    else
      reconstruct(vertices, a, i - 2, result + vertex)  // Case with the current vertex weighed more
  }
}

