package util

import scala.io.{BufferedSource, Source}

object Resources {

  def loadNumbers(name: String="inversions.txt"): Vector[Int] = {

    val s: BufferedSource = Source.fromResource(name)
    val array = s.getLines().map(_.toInt).toVector
    array
  }
}

