package assignments

import cs01.Karatsuba

object DoKaratsubaAssignment extends App {

  val r = Karatsuba.multiply(
    BigInt.apply("3141592653589793238462643383279502884197169399375105820974944592"),
    BigInt.apply("2718281828459045235360287471352662497757247093699959574966967627")
  )

  println(f"${r}")
}
