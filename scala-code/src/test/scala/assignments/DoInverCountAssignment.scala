package assignments

import cs01.InversionCounter

import scala.io.{BufferedSource, Source}
import util.Resources.loadNumbers

object DoInverCountAssignment extends App {

  val array = loadNumbers("inversions.txt")
  assert(array.size == 100000)

  val result = InversionCounter.countInversions(array)
  print(result)

 // print(InversionCounter.countInversions(Seq(1, 3, 5, 2, 4, 6)))

}
