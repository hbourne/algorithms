package unit.cs02

//import cs02.imperative.graph.{Edge, Graph, Kosaraju, Node}
//import unit.UnitSpec

//object ToyData {
//
//  val simpleToy = Seq(
//
//    // SCC 0, 1
//    0 -> Seq(1),  // 0 is the source
//
//    // SCC 2, 3
//    1 -> Seq(2),
//    2 -> Seq(3, 4),
//    3 -> Seq(2),
//
//    // SCC 4
//    4 -> Seq(5,6,7),
//    5 -> Seq(6),
//    6 -> Seq(7),
//    7 -> Seq(5, 8), // 8 is the sink. SCC 6
//  )
//
//
//  def exampleFromBook: Graph = {
//
//    val data = Array(
//      1 -> Seq(3),
//      3 -> Seq(5, 11),
//
//      11 -> Seq(6, 8),
//
//      6 -> Seq(10),
//      10 -> Seq(8),
//      8 -> Seq(6),
//
//      5 -> Seq(1, 7, 9),
//      9 -> Seq(2, 4, 8),
//      2 -> Seq(4, 10),
//      7 -> Seq(9),
//      4 -> Seq(7)
//    )
//
//    val correctZeros = data.map{
//      case (source, heads) => source - 1 -> heads.map(_  - 1)
//    }
//
//    fromNTuples(
//     correctZeros
//    )
//
//
//  }
//
//}
//
//}
//
//
//class KosarajuSpec extends UnitSpec {
//
//  "Example data" should "load" in {
//    val graph = ToyData.exampleFromBook
//
//    graph.nodes should have size 11
//    graph.edges should have length 18
//  }
//
//  "fromNtuples" should "correctly populate adjacency lists" in {
//
//    val spec = Seq(
//      0 -> Seq(1),
//      1 -> Seq(2),
//      2 -> Seq(3, 4),
//      4 -> Seq(5)
//    )
//    val graph = ToyData.fromNTuples(spec)
//
//    graph.nodes should have length 6
//    graph.edges should have length 5
//    graph.nodes.map(_.id) should contain(5)
//  }
//
//  "topoSort" should "correctly order an unambiguous graph" in {
//
//    val spec = ToyData.simpleToy
//    val graph = ToyData.fromNTuples(spec)
//
//    val result = Kosaraju.topoSortOuter(graph, graph.nodes.map(_.id))
//
//    // The sole source should be first in the ordering
//    result(0) shouldBe 0
//
//    // Node 4 should be explored before node 3,
//    //   as node 4 was a child of a prior node
//    result.indexOf(4) shouldBe < (result.indexOf(3))
//
//    // Node 7 should be explored before node 3,
//    //   as node 7 was a descendant of node 4
//    result.indexOf(7) shouldBe < (result.indexOf(3))
//  }
//
//
//  "Kosaraju" should "identify 4 SCCs in toy dataset" in {
//
//    val spec = ToyData.simpleToy
//    val graph = ToyData.fromNTuples(spec)
//
//    val sccs = Kosaraju.computeSCCs(graph)
//
//    sccs should have length 6
//  }
//
//  it should "identify 4 SCCs in Algos Illuminated example" in {
//
//    val graph = ToyData.exampleFromBook
//
//    val sccs = Kosaraju.computeSCCs(graph)
//
//    sccs should have length 4
//  }
//}
