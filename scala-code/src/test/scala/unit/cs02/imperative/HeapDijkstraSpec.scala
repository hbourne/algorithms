package cs02.imperative

import cs02.imperative.Heap
import unit.UnitSpec

import scala.collection.mutable.ArrayBuffer

class HeapSpec extends UnitSpec {

  val emptyArray = new ArrayBuffer[(Int, Int)]()

  //  For tests where I'm using the value of a number as its key
  implicit class ArrayOps[A](underlying: ArrayBuffer[A]) {
    def zipSelf: ArrayBuffer[(A, A)] = underlying zip underlying
  }

  "Accessing a parent" should "return None for a {0, 1} sized Heap"  in {
    Heap.parent(emptyArray, 0) shouldBe None
    Heap.parent(emptyArray, 1) shouldBe None
    Heap.parent(emptyArray, 10) shouldBe None
  }

  it should "return expected parent in examples" in {
    val data = ArrayBuffer(1, 10, 2, 15).zipSelf

    Heap.parent(data, 0) shouldBe None
    Heap.parent(data, 1) shouldBe Some(0 -> 1)
    Heap.parent(data, 2) shouldBe Some(0 -> 1)
    Heap.parent(data, 3) shouldBe Some(1 -> 10)
  }

  "Accessing a child" should "return None where none exists" in {
    Heap.childLeft(emptyArray, 0) shouldBe None
    Heap.childRight(emptyArray, 0) shouldBe None

    val data = ArrayBuffer(1, 10, 2, 15).zipSelf
    Heap.childLeft(data, 2) shouldBe None
    Heap.childRight(data, 2) shouldBe None

    Heap.childLeft(data, 1) shouldBe defined
    Heap.childRight(data, 1) shouldBe None
  }

  it should "return correct values" in {
    val data = ArrayBuffer(1, 10, 2, 15).zipSelf
    Heap.childLeft(data, 0) shouldBe Some(1, 10)
    Heap.childRight(data, 0) shouldBe Some(2, 2)
    Heap.childLeft(data, 1) shouldBe Some(3, 15)
  }

  "Array swapping" should "work as expected" in {
    val data = ArrayBuffer(0,1,2,3,4,5,6,7).zipSelf

    Heap.swap(data, 0, 1)
    data.take(2).map(_._1) should contain theSameElementsInOrderAs Seq(1,0)

    Heap.swap(data, 4, 7)
    data(7)._1 shouldBe 4
    data(4)._1 shouldBe 7
  }

  "BubbleUp" should "raise a given element to its rightful place" in {
    val data = ArrayBuffer(10, 10, 10, 1).zipSelf
    Heap.bubbleUp(data, 3)
    data.map(_._1) should contain theSameElementsInOrderAs Seq(1, 10, 10, 10)
  }

  it should "raise an element in a larger example" in {
    val data = ArrayBuffer(
                     9,
             10,           10,
         11,    11,     11,     11,
       12, 12, 12, 12, 12, 12, 12, 12,
     1
    ).zipSelf

    Heap.bubbleUp(data, data.indexOf(1 -> 1))
    data.head._1 shouldBe 1
  }

  "BubbleDown" should "lower an element to its righful place" in {
    val data = ArrayBuffer(10, 10, 1).zipSelf
    Heap.bubbleDown(data, 0)
    data.map(_._1) should contain theSameElementsInOrderAs Seq(1, 10, 10)
  }

  it should "lower an element in a larger example" in {
    val data = ArrayBuffer(
                   900,
             10,            10,
         11,    11,     11,     11,
       12, 12, 12, 12, 12, 12, 12, 12,
     13
    ).zipSelf
    Heap.bubbleDown(data, 0)
    data.last._1 shouldBe 900
  }

  implicit def heapData[K, V](heap: Heap[K, V]): ArrayBuffer[(K, V)] = heap.data

  def intHeap: Heap[Int, Int] = new Heap[Int, Int]()

  "A Heap" should "insert a root element" in {
    val heap = intHeap

    heap.data.headOption shouldBe None
    heap.insert(1, 1)

    heap.data should contain (1,1)

    Heap.parent(heap, 0) shouldBe None
    Heap.childLeft(heap, 0) shouldBe None
    Heap.childRight(heap, 0) shouldBe None
  }

  it should "insert one child correctly" in {
    val heap = intHeap

    heap.insert(5, 5)
    heap.insert(10, 10)
    heap.data.map(_._1) should contain theSameElementsInOrderAs Seq(5,10)

    Heap.childLeft(heap, 0) shouldBe Some(1 -> 10)
    Heap.childRight(heap, 0) shouldBe None
    Heap.parent(heap, 1) shouldBe Some(0 -> 5)
  }

   it should "insert 2nd child correctly" in {
    val heap = intHeap

    heap.insert(5, 5)
    heap.insert(10, 10)
    heap.insert(7, 7)

    heap.data.map(_._1) should contain theSameElementsInOrderAs Seq(5,10, 7)

    Heap.childLeft(heap, 0) shouldBe Some(1 -> 10)
    Heap.childRight(heap, 0) shouldBe Some(2 -> 7)
    Heap.parent(heap, 1) shouldBe Some(0 -> 5)
  }

  it should "swap elements if invariant is broken" in {
    val heap = intHeap

    heap.insert(5, 5)
    heap.data.head._1 shouldBe 5

    heap.insert(10, 10)
    heap.data.map(_._1) should contain theSameElementsInOrderAs Seq(5, 10)
  }

  "Heapify" should "replicate multiple inserts" in {

    val heap = Heap(Seq(5-> 5, 10 -> 10, 7 -> 7))

    heap.data should contain theSameElementsInOrderAs Seq(
      (5,5) ,(10, 10), (7,7))

    Heap.childLeft(heap, 0) shouldBe Some(1 -> 10)
    Heap.childRight(heap, 0) shouldBe Some(2 -> 7)
    Heap.parent(heap, 1) shouldBe Some(0 -> 5)
  }

  it should "populate deeper levels" in {

 //   val heap = Heap(Seq(20, 10, 8, 2, 1))

    // todo some kind of recursive invariant checker?
    // Bit fiddly, passing for now.
  }

  "ExtractMin" should "extract the smallest element" in {
    val heap = Heap(ArrayBuffer(20, 10, 8, 2, 1).zipSelf.toSeq)
    heap.extractMin() shouldBe Some(1 -> 1)
  }

  it should "return None for an empty Heap" in {
    Heap[Int, Int](Nil).extractMin() shouldBe None
  }

  it should "balance the books after extracting the root" in {
    val heap = Heap(ArrayBuffer(
                     1,
             2,             10,
          4 ,    11,     11,     11,
        6,  6, 12, 12, 12, 12, 12, 12,
     // ^
      13).zipSelf.toSeq
    )

    heap.extractMin()

    heap(0)._1 shouldBe 2
    heap(1)._1 shouldBe 4
    heap(3)._1 shouldBe 6

    /*
    We should see the result of 13 being set as root node, and
    then bubbleDown'ed. As bubble down chooses the smallest child
    to travel down, this follows the left most path on our tree
     */

    val indexOf13 = heap.indexOf((13, 13))
    indexOf13 shouldEqual 7
  }

}


