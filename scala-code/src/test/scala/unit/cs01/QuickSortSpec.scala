package cs01

import cs01.QuickSort._
import cs01.{InversionCounter, PivotRules, QuickSort}
import unit.UnitSpec
import util.Resources


class QuickSortSpec extends UnitSpec {

  "Array swapping" should "perform as expected" in {
    val array = Array(0, 1, 2, 3)
    array.swapElements(0, 3)
    array should contain theSameElementsInOrderAs Array(3, 1, 2, 0)
  }

  "Randomised choose pivot" should "throw expected numbers" in {
   PivotRules.random(10, -1, Array()) should equal(5 +- 5)
  }

  it should "gracefully handle zero, one" in {
    PivotRules.random(0,0, Array()) shouldBe 0
    PivotRules.random(0,1, Array(1)) shouldBe 1
  }

  "Partition" should "reverse a backwards size 2 array" in {
    val biArray = Array(2, 1)
    partition(biArray,
      left=0,
      right=1,
      forcedPivot = Some(1))

    biArray should contain theSameElementsInOrderAs Array(1, 2)
  }

  it should "successfully swap elements 0 and 2, when forced to pivot around element 1" in {
    val array = Array(10, 3, 0)
    partition(array,
      left=0,
      right=2,
      forcedPivot=Some(1)  // Force use of "3"
    )

    array should contain theSameElementsInOrderAs Array(0, 3, 10)
  }

  it should "pivot a larger array as expected" in {
    val array = Array(15, 10, 20, 8, 5, 1, 2)
    partition(array, 0, 6, Some(3))

    // Not quite the order I expected, but satisfies pivot condition
    array should contain theSameElementsInOrderAs Array(2, 5, 1, 8, 10, 20, 15)
  }

  it should "increase the sortedness of some random arrays" in {

    val a = Array(5, 4, 3, 2, 1, 0) // reversed totally
    val b = Array(5, 2, 3, 4, 1.0)  // slightly better
    val c = Array(0, 1, 2, 3, 4, 5) // perfect sort

    // Confirm #inversions higher for less sorted cases
    val aInv = InversionCounter.countInversions(a)
    val bInv = InversionCounter.countInversions(b)
    val cInv = InversionCounter.countInversions(c)

    assert(aInv > bInv, "Reverse order should have more inversions that partial order")
    assert(bInv > cInv, "Partial order should have more inversions than ordered")

    // Sort partially ordered array
    QuickSort.partition(b, 0, 4)

    val bInvNew = InversionCounter.countInversions(b)
    assert(bInv > bInvNew, "One pass of partition should have reduced the number of inversions")
  }

  "Quicksort" should "sort!" in {
    val testCases = Map(
      Array(0, 10, 2) -> Array(0, 2, 10),
      Array(2, 0, 10) -> Array(0, 2, 10),
      Array(10, 0, 2) -> Array(0, 2, 10),
      Array(0, 2, 10) -> Array(0, 2, 10),
    )

    for ((input, expectedOutcome) <- testCases) yield
      QuickSort.sortWithCounts(input)._1 should contain theSameElementsInOrderAs expectedOutcome
  }

  object TestCaseOne {
    val data = Array(
      2148, 9058, 7742, 3153, 6324, 609, 7628, 5469, 7017, 504
    )
  }

  "Test case 1" should "produce a sorted result" in {

    val (res, nIter) = QuickSort.sortWithCounts(TestCaseOne.data)
    val expectedOutcome = Array(
      504, 609, 2148, 3153, 5469, 6324, 7017, 7628, 7742, 9058
    )

    res should contain theSameElementsInOrderAs expectedOutcome
  }

  it should "always report comparisons within {n log(n) < x < n*n}" in {
    val iterationReports = Range(0,100).map(_ => QuickSort.sortWithCounts(TestCaseOne.data)._2)

    val n = TestCaseOne.data.length

    val lower = math.floor(n * math.log(n))
    val upper = n * n

    val withinBounds = (i: Int) => i >= lower && i <= upper
    val failures = iterationReports filterNot withinBounds

    println(s"Mean iter: ${(iterationReports.sum / iterationReports.length)}")
    failures shouldBe empty
  }

  it should "report 25 comparisons expected when using first element rule" in {
    val nIter = QuickSort.sortWithCounts(
      TestCaseOne.data,
      pivotRule = PivotRules.first
    )._2

    nIter shouldEqual 25
  }

  it should "report 31 comparisons expected when using last element rule" in {
    val nIter = QuickSort.sortWithCounts(
      TestCaseOne.data,
      pivotRule = PivotRules.last
    )._2

    nIter shouldEqual 31
  }

  "Test case 2" should "take 620" in {
     val data: Array[Int] = Resources.loadNumbers("quicksort/TestCase2.txt").toArray

     val nIter = QuickSort.sortWithCounts(
      data,
      PivotRules.first
    )._2

    nIter shouldEqual 620



  }
}
