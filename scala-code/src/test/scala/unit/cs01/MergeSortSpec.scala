package cs01

import cs01.MergeSort._
import unit.UnitSpec

class MergeSortSpec extends UnitSpec {

  "Seq combine op" should "accrue elements based on size" in {
    val result = combine(
      Seq(1,1,1,4), Seq(3,6,8,10)
    )
    result should contain theSameElementsInOrderAs Seq(1,1,1,3,4,6,8,10)
  }

  "sort operation" should "sort base cases" in {
    sort(Seq(2,1)) should contain theSameElementsInOrderAs Seq(1,2)
  }

  it should "sort odd size base cases too" in {
    sort(Seq(2,1,3)) should contain theSameElementsInOrderAs Seq(1,2,3)
  }

  it should "sort larger case" in {
    val input = Seq(7, 1, 8, 3, 7, 9, 32, 8, 2, 6)
    val result = sort(input)

    result should contain theSameElementsInOrderAs(input.sorted)
  }
}
