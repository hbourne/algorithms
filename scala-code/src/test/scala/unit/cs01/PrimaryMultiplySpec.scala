package cs01

import cs01.PrimarySchoolMultiply._
import unit.UnitSpec

class PrimaryMultiplySpec extends UnitSpec {

  "Number to array converter" should "give expected result" in {
    val result = base10Array(100)
    assert(result == Seq(1, 0, 0))
  }

  "Base ten ifier" should "give expected result" in {

    val inputs = Seq(
      Seq(1, 1),
      Seq(1, 11),
      Seq(1, 21),
      Seq(58, 261)
    )

    val outcomes = inputs.map(baseTenify(_))

    val expectedOutputs = Seq(
      Seq(1, 1),
      Seq(2, 1),
      Seq(3, 1),
      Seq(8, 4, 1)
    )

    for {
      (result, expectedValue) <- outcomes zip expectedOutputs
    } yield {
      assert(result == expectedValue)
    }
  }

  "Multiply" should "nail a couple examples" in {
    multiply(2, 4) shouldEqual 8
    multiply(22, 2) shouldEqual 44
    multiply(29, 29) shouldEqual 841
    multiply(1234, 5678) shouldEqual 7006652
  }

}
