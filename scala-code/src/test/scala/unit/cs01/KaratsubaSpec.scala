package cs01

import cs01.Karatsuba
import unit.UnitSpec

class KaratsubaSpec extends UnitSpec {

  // Realise these kinds of "behave" tests are evil
  // However this class was created afterward

  "Base10Array" should "behave" in {
    Karatsuba.tobase10Array(3804124) should contain theSameElementsInOrderAs Seq(3,8,0,4,1,2,4)
  }

  "Converting to base 10" should "behave" in {
    Karatsuba.fromBase10Array(List(3,8,0,4,1,2,4)) shouldBe 3804124

    Karatsuba.fromBase10Array(
      Karatsuba.tobase10Array(3804124)
    ) shouldBe 3804124
  }

  "A simple multiply case" should "behave" in {
    Karatsuba.multiply(1234, 5678) shouldBe 7006652
  }

}
