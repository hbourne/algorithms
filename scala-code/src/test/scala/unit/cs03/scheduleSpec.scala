package unit.cs03

import cs03.greedy.{ExhaustiveScheduler, GreedyDiff, GreedyRatio, Job, ScheduleObjective}
import unit.UnitSpec

class scheduleSpec extends UnitSpec {

  "Min by weighted scores" should "yield correct answers" in {

    val inputs = Seq(
      Seq(Job(1, 1)),
      Seq(Job(1, 1), Job(2, 1)),
      Seq(Job(1, 1), Job(2, 1), Job(3, 1))
    )

    val expectedResults = Seq(
       1,
       4, // 1 + (1 * 3)
      10  // 1 + 4 + (1 * 6)
    )

    for ((input, expected) <- inputs zip expectedResults) yield
      ScheduleObjective.minWeightedCompletionTimes(input) shouldEqual expected
  }

  "ExhaustiveScheduler" should "find optimal case" in {
    val rawJobs = Seq(Job(1,1), Job(2,1), Job(3,1))
    val result = ExhaustiveScheduler.plan(rawJobs)
    val C = ScheduleObjective.minWeightedCompletionTimes(result.get)
    C shouldBe 10
  }

  "Comparisons..." should "find ~optimal case" in {
    val rawJobs = Seq(Job(1,1),Job(2,1), Job(3,1))
    for {
      exhaust <- ExhaustiveScheduler.plan(rawJobs)
      gDiff <- GreedyDiff.plan(rawJobs)
      gRatio <- GreedyRatio.plan(rawJobs)
    } yield {
      val cExhaust = ScheduleObjective.minWeightedCompletionTimes(exhaust)
      val cGdiff = ScheduleObjective.minWeightedCompletionTimes(gDiff)
      val cGratio = ScheduleObjective.minWeightedCompletionTimes(gRatio)

      cExhaust shouldBe 10
      cGdiff shouldBe 10
      cGratio shouldBe 10
    }
  }

}
